'use strict'

import { ComponentAPI, Variable, VariableDefinitionType } from '@ayana/bento'
import { Logger } from '@ayana/logger'

import * as DiscordJS from 'discord.js'

const log = Logger.get('ClientComponent')

export class ClientComponent {
	public api!: ComponentAPI
    public name: string = 'ClientComponent'

    //defining new instance of Discord
	private cli!: DiscordJS.Client

	public async onLoad() {
        log.info('loaded ClientComponent')
        //creating new instance of Discord Client
		this.cli = new DiscordJS.Client()
		// Forward Discord.JS events to all Components
		this.api.forwardEvents(this.cli, ['ready', 'message'])
		this.cli.login(this.api.getVariable('TOKEN'))
	}

	public get getCli(): DiscordJS.Client {
		return this.cli
	}
}

'use strict'

import { ComponentAPI, SubscribeEvent } from '@ayana/bento'
import { EventEmitter } from 'events'
import { Logger } from '@ayana/logger'

import { ClientComponent } from './ClientComponent'
import { Message } from 'discord.js'

const log = Logger.get('CommandsComponent')

export class CommandsComponent {
	public api!: ComponentAPI
	public name: string = 'CommandsComponent'
	private emitter: EventEmitter = new EventEmitter()

	public async onLoad() {
		log.info('loaded CommandsComponent')
		this.api.forwardEvents(this.emitter, ['command'])
	}

	public async onChildLoad() {
		log.info('loaded one of the child from CommandsComponent')
	}

	@SubscribeEvent(ClientComponent, 'ready')
	private handleReady() {
		log.info('logged succesfully into Discord!')
	}

	@SubscribeEvent(ClientComponent, 'message')
	private handleMessage(msg: Message) {
		if (msg.toString().startsWith('/')) {
			const args: string[] = msg.toString().replace('/', '').split(' ')
			const cmd: string = args[0]
			this.emitter.emit('command', [cmd, args])
		}
	}
}

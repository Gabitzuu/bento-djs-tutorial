'use strict'

import { ComponentAPI, SubscribeEvent, ChildOf } from '@ayana/bento'
import { Logger } from '@ayana/logger'

import { CommandsComponent } from '../CommandsComponent'

const log = Logger.get('TestCommand')

export class TestCommand {
	public api!: ComponentAPI
	public name: string = 'TestCommand'
	public parent: string = 'CommandsComponent'

	public async onLoad() {
		log.info('loaded TestCommand')
	}

	@SubscribeEvent(CommandsComponent, 'command')
	private handleCommand(msg: string, args: string[]) {
		log.warn(msg)
	}
}

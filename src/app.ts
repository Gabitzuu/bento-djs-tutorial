import { Bento, FSComponentLoader, ConfigLoader } from '@ayana/bento'
import { Logger } from '@ayana/logger'

const log = Logger.get('App')
const bento = new Bento();

(async () => {
	const fsloader = new FSComponentLoader()
	await fsloader.addDirectory(__dirname, 'components')

    // injecting the application TOKEN, used for logging into discord
	bento.setVariable(
		'TOKEN',
		'!!!!!!!!!YOUR TOKEN HERE!!!!!!!!!'
	)
	// attach plugins and verify bento state
	await bento.addPlugins([fsloader])
	await bento.verify()
})().catch(e => {
	log.error("Ok something it's wrong here, fix that")
	log.error(e)
	process.exit(1)
})
